<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class ServeOnPort3005 extends Command
{
    protected $signature = 'serve:3005';

    protected $description = 'Serve the application on port 3005';

    public function handle()
    {
        $this->info('Laravel development server started on http://127.0.0.1:3005/api/newMovie');

        $process = new Process(['php', 'artisan', 'serve', '--port=3005']);
        $process->setTimeout(null);
        $process->run(function ($type, $line) {
            $this->output->write($line);
        });
    }
}


