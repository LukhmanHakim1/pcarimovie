<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use DateTime;
use Illuminate\Http\Request;

class MoviesController
{
    public function index()
    {

            $movies = [
                [
                    "Movie_ID" => 2,
                    "Title" => "Parasite",
                    "Genre" => "Comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BYWZjMjk3ZTItODQ2ZC00NTY5LWE0ZDYtZTI3MjcwN2Q5NTVkXkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg",
                    "Overall_rating" => 7.2,
                    "Description" => "A poor family, the Kims, con their way into becoming the servants of a rich family, the Parks. But their easy life gets complicated when their deception is threatened with exposure."
                ],
                [
                    "Movie_ID" => 3,
                    "Title" => "The Favourite",
                    "Genre" => "Comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMTg1NzQwMDQxNV5BMl5BanBnXkFtZTgwNDg2NDYyNjM@._V1_.jpg",
                    "Overall_rating" => 7.0,
                    "Description" => "In early 18th century England, a frail Queen Anne occupies the throne and her close friend, Lady Sarah, governs the country in her stead. When a new servant, Abigail, arrives, her charm endears her to Sarah."
                ],
                [
                    "Movie_ID" => 4,
                    "Title" => "Inception",
                    "Genre" => "Sci-Fi",
                    "Duration" => "2 hours 28 minutes",
                    "Views" => "35.6k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SY1000_CR0,0,674,1000_AL_.jpg",
                    "Overall_rating" => 8.8,
                    "Description" => "A thief who enters the dreams of others to steal secrets from their subconscious gets the idea to plant an idea into the mind of a CEO."
                ],
                [
                    "Movie_ID" => 5,
                    "Title" => "The Shawshank Redemption",
                    "Genre" => "Drama",
                    "Duration" => "2 hours 22 minutes",
                    "Views" => "42.9k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg",
                    "Overall_rating" => 9.3,
                    "Description" => "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency."
                ],
                [
                    "Movie_ID" => 6,
                    "Title" => "The Dark Knight",
                    "Genre" => "Action",
                    "Duration" => "2 hours 32 minutes",
                    "Views" => "38.2k",
                    "Poster" => "https://m.media-amazon.com/images/I/51cYhfudPZL._AC_.jpg",
                    "Overall_rating" => 9.0,
                    "Description" => "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice."
                ],
                [
                    "Movie_ID" => 7,
                    "Title" => "Interstellar",
                    "Genre" => "Sci-Fi",
                    "Duration" => "2 hours 49 minutes",
                    "Views" => "29.7k",
                    "Poster" => "https://m.media-amazon.com/images/I/51Iwg2dBjNL._AC_.jpg",
                    "Overall_rating" => 8.6,
                    "Description" => "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival."
                ],
                [
                    "Movie_ID" => 8,
                    "Title" => "Pulp Fiction",
                    "Genre" => "Crime",
                    "Duration" => "2 hours 34 minutes",
                    "Views" => "31.5k",
                    "Poster" => "https://m.media-amazon.com/images/I/41ThIdEvugL._AC_.jpg",
                    "Overall_rating" => 8.9,
                    "Description" => "The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption."
                ],
                [
                    "Movie_ID" => 9,
                    "Title" => "Fight Club",
                    "Genre" => "Drama",
                    "Duration" => "2 hours 19 minutes",
                    "Views" => "28.6k",
                    "Poster" => "https://m.media-amazon.com/images/I/81+KV3A2a8L._AC_SY679_.jpg",
                    "Overall_rating" => 8.8,
                    "Description" => "An insomniac office worker and a devil-may-care soapmaker form an underground fight club that evolves into something much, much more."
                ],
                [
                    "Movie_ID" => 10,
                    "Title" => "Forrest Gump",
                    "Genre" => "Drama",
                    "Duration" => "2 hours 22 minutes",
                    "Views" => "40.3k",
                    "Poster" => "https://m.media-amazon.com/images/I/51v5ZpFyaFL._AC_SY679_.jpg",
                    "Overall_rating" => 8.8,
                    "Description" => "The presidencies of Kennedy and Johnson, the Vietnam War, the Watergate scandal and other historical events unfold from the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart."
                ],
                [
                    "Movie_ID" => 11,
                    "Title" => "The Favourite",
                    "Genre" => "comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMTg1NzQwMDQxNV5BMl5BanBnXkFtZTgwNDg2NDYyNjM@._V1_.jpg",
                    "Overall_rating" => 7,
                    "Description" => "In early 18th century England, a frail Queen Anne occupies the throne and her close friend, Lady Sarah, governs the country in her stead. When a new servant, Abigail, arrives, her charm endears her to Sarah."
                ],
                [
                    "Movie_ID" => 12,
                    "Title" => "The Farewell I",
                    "Genre" => "comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMWE3MjViNWUtY2VjYS00ZDBjLTllMzYtN2FkY2QwYmRiMDhjXkEyXkFqcGdeQXVyODQzNTE3ODc@._V1_.jpg",
                    "Overall_rating" => 6.9,
                    "Description" => "A Chinese family discovers their grandmother has only a short while left to live and decide to keep her in the dark, scheduling a wedding to gather before she dies."
                ],
                [
                    "Movie_ID" => 13,
                    "Title" => "Marriage Story",
                    "Genre" => "comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BZGVmY2RjNDgtMTc3Yy00YmY0LTgwODItYzBjNWJhNTRlYjdkXkEyXkFqcGdeQXVyMjM4NTM5NDY@._V1_.jpg",
                    "Overall_rating" => 8.8,
                    "Description" => "Noah Baumbach's incisive and compassionate look at a marriage breaking up and a family staying together."
                ],
                [
                    "Movie_ID" => 14,
                    "Title" => "Booksmart",
                    "Genre" => "comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BYzE0ZTY1NGUtOTYxMS00NWYzLWE1NGMtMDU3YzViZDZjZTZkXkEyXkFqcGdeQXVyMTA4NjE0NjEy._V1_.jpg",
                    "Overall_rating" => 8.2,
                    "Description" => "On the eve of their high school graduation, two academic superstars and best friends realize they should have worked less and played more. Determined not to fall short of their peers, the girls try to cram four years of fun into one night."
                ]
            ];

            return response()->json(['data' => $movies]);

    }

    public function searchMovie(Request $request)
    {

            $movies = [
                [
                    "Movie_ID" => 2,
                    "Title" => "Parasite",
                    "Genre" => "Comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BYWZjMjk3ZTItODQ2ZC00NTY5LWE0ZDYtZTI3MjcwN2Q5NTVkXkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg",
                    "Overall_rating" => 7.2,
                    "Description" => "A poor family, the Kims, con their way into becoming the servants of a rich family, the Parks. But their easy life gets complicated when their deception is threatened with exposure."
                ],
                [
                    "Movie_ID" => 3,
                    "Title" => "The Favourite",
                    "Genre" => "Comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMTg1NzQwMDQxNV5BMl5BanBnXkFtZTgwNDg2NDYyNjM@._V1_.jpg",
                    "Overall_rating" => 7.0,
                    "Description" => "In early 18th century England, a frail Queen Anne occupies the throne and her close friend, Lady Sarah, governs the country in her stead. When a new servant, Abigail, arrives, her charm endears her to Sarah."
                ],
                [
                    "Movie_ID" => 4,
                    "Title" => "Inception",
                    "Genre" => "Sci-Fi",
                    "Duration" => "2 hours 28 minutes",
                    "Views" => "35.6k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SY1000_CR0,0,674,1000_AL_.jpg",
                    "Overall_rating" => 8.8,
                    "Description" => "A thief who enters the dreams of others to steal secrets from their subconscious gets the idea to plant an idea into the mind of a CEO."
                ],
                [
                    "Movie_ID" => 5,
                    "Title" => "The Shawshank Redemption",
                    "Genre" => "Drama",
                    "Duration" => "2 hours 22 minutes",
                    "Views" => "42.9k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg",
                    "Overall_rating" => 9.3,
                    "Description" => "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency."
                ],
                [
                    "Movie_ID" => 6,
                    "Title" => "The Dark Knight",
                    "Genre" => "Action",
                    "Duration" => "2 hours 32 minutes",
                    "Views" => "38.2k",
                    "Poster" => "https://m.media-amazon.com/images/I/51cYhfudPZL._AC_.jpg",
                    "Overall_rating" => 9.0,
                    "Description" => "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice."
                ],
                [
                    "Movie_ID" => 7,
                    "Title" => "Interstellar",
                    "Genre" => "Sci-Fi",
                    "Duration" => "2 hours 49 minutes",
                    "Views" => "29.7k",
                    "Poster" => "https://m.media-amazon.com/images/I/51Iwg2dBjNL._AC_.jpg",
                    "Overall_rating" => 8.6,
                    "Description" => "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival."
                ],
                [
                    "Movie_ID" => 8,
                    "Title" => "Pulp Fiction",
                    "Genre" => "Crime",
                    "Duration" => "2 hours 34 minutes",
                    "Views" => "31.5k",
                    "Poster" => "https://m.media-amazon.com/images/I/41ThIdEvugL._AC_.jpg",
                    "Overall_rating" => 8.9,
                    "Description" => "The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption."
                ],
                [
                    "Movie_ID" => 9,
                    "Title" => "Fight Club",
                    "Genre" => "Drama",
                    "Duration" => "2 hours 19 minutes",
                    "Views" => "28.6k",
                    "Poster" => "https://m.media-amazon.com/images/I/81+KV3A2a8L._AC_SY679_.jpg",
                    "Overall_rating" => 8.8,
                    "Description" => "An insomniac office worker and a devil-may-care soapmaker form an underground fight club that evolves into something much, much more."
                ],
                [
                    "Movie_ID" => 10,
                    "Title" => "Forrest Gump",
                    "Genre" => "Drama",
                    "Duration" => "2 hours 22 minutes",
                    "Views" => "40.3k",
                    "Poster" => "https://m.media-amazon.com/images/I/51v5ZpFyaFL._AC_SY679_.jpg",
                    "Overall_rating" => 8.8,
                    "Description" => "The presidencies of Kennedy and Johnson, the Vietnam War, the Watergate scandal and other historical events unfold from the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart."
                ],
                [
                    "Movie_ID" => 11,
                    "Title" => "The Favourite",
                    "Genre" => "comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMTg1NzQwMDQxNV5BMl5BanBnXkFtZTgwNDg2NDYyNjM@._V1_.jpg",
                    "Overall_rating" => 7,
                    "Description" => "In early 18th century England, a frail Queen Anne occupies the throne and her close friend, Lady Sarah, governs the country in her stead. When a new servant, Abigail, arrives, her charm endears her to Sarah."
                ],
                [
                    "Movie_ID" => 12,
                    "Title" => "The Farewell I",
                    "Genre" => "comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BMWE3MjViNWUtY2VjYS00ZDBjLTllMzYtN2FkY2QwYmRiMDhjXkEyXkFqcGdeQXVyODQzNTE3ODc@._V1_.jpg",
                    "Overall_rating" => 6.9,
                    "Description" => "A Chinese family discovers their grandmother has only a short while left to live and decide to keep her in the dark, scheduling a wedding to gather before she dies."
                ],
                [
                    "Movie_ID" => 13,
                    "Title" => "Marriage Story",
                    "Genre" => "comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BZGVmY2RjNDgtMTc3Yy00YmY0LTgwODItYzBjNWJhNTRlYjdkXkEyXkFqcGdeQXVyMjM4NTM5NDY@._V1_.jpg",
                    "Overall_rating" => 8.8,
                    "Description" => "Noah Baumbach's incisive and compassionate look at a marriage breaking up and a family staying together."
                ],
                [
                    "Movie_ID" => 14,
                    "Title" => "Booksmart",
                    "Genre" => "comedy",
                    "Duration" => "1 hour 20 minutes",
                    "Views" => "21.1k",
                    "Poster" => "https://m.media-amazon.com/images/M/MV5BYzE0ZTY1NGUtOTYxMS00NWYzLWE1NGMtMDU3YzViZDZjZTZkXkEyXkFqcGdeQXVyMTA4NjE0NjEy._V1_.jpg",
                    "Overall_rating" => 8.2,
                    "Description" => "On the eve of their high school graduation, two academic superstars and best friends realize they should have worked less and played more. Determined not to fall short of their peers, the girls try to cram four years of fun into one night."
                ]
            ];

        // Assigning the initial set of movies to filteredResults
        $filteredResults = $movies;

        // Retrieving query parameters from the request
        $date         = $request->query('date');
        $dateEnd      = $request->query('dateEnd');
        $theaterName  = $request->query('theater_name');
        $title        = $request->query('title');

        $movies = [
            [
                "Movie_ID"=> 1,
                "Title"=> "The Irishman",
                "Duration"=> "1 hour 20 minutes",
                "Views"=> "21.1k",
                "Genre"=> "Comedy",
                "Poster"=> "https://m.media-amazon.com/images/M/MV5BMGUyM2ZiZmUtMWY0OC00NTQ4LThkOGUtNjY2NjkzMDJiMWMwXkEyXkFqcGdeQXVyMzY0MTE3NzU@._V1_FMjpg_UX1000_.jpg",
                "Overall_rating"=> 7.9,
                "Theater_name"=> "ABC Movies",
                "Start_time"=> "2020-04-04T09:00:00",
                "End_time"=> "2020-04-04T12:00:00",
                "Description"=> "An aging hitman recalls his time with the mob and the intersecting events with his friend, Jimmy Hoffa, through the 1950-70s.",
                "Theater_room_no"=> 1
            ],
            [
                "Movie_ID"=> 3,
                "Title"=> "The Favourite",
                "Duration"=> "1 hour 20 minutes",
                "Views"=> "21.1k",
                "Genre"=> "Comedy",
                "Poster"=> "https://m.media-amazon.com/images/M/MV5BMTg1NzQwMDQxNV5BMl5BanBnXkFtZTgwNDg2NDYyNjM@._V1_.jpg",
                "Overall_rating"=> 7.0,
                "Theater_name"=> "ABC Movies",
                "Start_time"=> "2020-04-04T11:00:00",
                "End_time"=> "2020-04-04T14:00:00",
                "Description"=> "In early 18th century England, a frail Queen Anne occupies the throne and her close friend, Lady Sarah, governs the country in her stead. When a new servant, Abigail, arrives, her charm endears her to Sarah.",
                "Theater_room_no"=> 3
            ],
            [
                "Movie_ID"=> 5,
                "Title"=> "Shoplifters",
                "Duration"=> "1 hour 20 minutes",
                "Views"=> "21.1k",
                "Genre"=> "Comedy",
                "Poster"=> "https://m.media-amazon.com/images/M/MV5BYWZmOTY0MDAtMGRlMS00YjFlLWFkZTUtYmJhYWNlN2JjMmZkXkEyXkFqcGdeQXVyODAzODU1NDQ@._V1_.jpg",
                "Overall_rating"=> 8.9,
                "Theater_name"=> "ABC Movies",
                "Start_time"=> "2020-04-20T09:00:00",
                "End_time"=> "2020-04-20T12:00:00",
                "Description"=> "A family of small-time crooks take in a child they find outside in the cold.",
                "Theater_room_no"=> 5
            ],
            [
                "Movie_ID"=> 2,
                "Title"=> "Parasite",
                "Duration"=> "1 hour 20 minutes",
                "Views"=> "21.1k",
                "Genre"=> "Comedy",
                "Poster"=> "https://m.media-amazon.com/images/M/MV5BYWZjMjk3ZTItODQ2ZC00NTY5LWE0ZDYtZTI3MjcwN2Q5NTVkXkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg",
                "Overall_rating"=> 7.2,
                "Theater_name"=> "ABC Movies",
                "Start_time"=> "2020-04-04T09:00:00",
                "End_time"=> "2020-04-04T12:00:00",
                "Description"=> "A poor family, the Kims, con their way into becoming the servants of a rich family, the Parks. But their easy life gets complicated when their deception is threatened with exposure.",
                "Theater_room_no"=> 2
            ],
            [
                "Movie_ID"=>4,
                "Title"=>"The Farewell I",
                "Duration"=>"1 hour 20 minutes",
                "Views"=>"21.1k",
                "Genre"=>"comedy",
                "Poster"=>"https://m.media-amazon.com/images/M/MV5BMWE3MjViNWUtY2VjYS00ZDBjLTllMzYtN2FkY2QwYmRiMDhjXkEyXkFqcGdeQXVyODQzNTE3ODc@._V1_.jpg",
                "Overall_rating"=>6.9,
                "Theater_name"=>"ABC movies",
                "Start_time"=>"2020-04-04T12:00:00",
                "End_time"=>"2020-04-04T15:00:00",
                "Description"=>"A Chinese family discovers their grandmother has only a short while left to live and decide to keep her in the dark, scheduling a wedding to gather before she dies.",
                "Theater_room_no"=>4
            ]
        ];


        // Assigning the initial set of movies to filteredResults
        $filteredResults = $movies;

        // Retrieving query parameters from the request
        $date           = $request->query('date');
        $dateEnd        = $request->query('dateEnd');
        $theaterName    = $request->query('theater_name');
        $title          = $request->query('title');

        // Filtering based on start date if only 'date' is provided
        if ($date !== null && $dateEnd === null) {
            $filteredResults = array_filter($movies, function($movie) use ($date) {
                // Extracting and formatting the start time of each movie
                $formattedStartTime = ($movie['Start_time']);
                // Checking if the formatted start time contains the provided date
                return strpos($formattedStartTime, $date) !== false;
            });
        }

        // Further filtering based on theater name and date
        if ($theaterName !== null && $date !== null) {
            // First filter based on date
            $filteredResults = array_filter($movies, function($movie) use ($date) {
                // Extracting and formatting the start time of each movie
                $formattedStartTime = ($movie['Start_time']);
                // Checking if the formatted start time contains the provided date
                return strpos($formattedStartTime, $date) !== false;
            });

            // Second filter based on theater name
            $filteredResults = array_filter($filteredResults, function($movie) use ($theaterName) {
                // Checking if the theater name matches (case-insensitive)
                return stripos($movie['Theater_name'], $theaterName) !== false;
            });
        }

        // Further filtering based only on theater name
        if ($theaterName !== null) {
            $filteredResults = array_filter($filteredResults, function($movie) use ($theaterName) {
                // Checking if the theater name matches (case-insensitive)
                return stripos($movie['Theater_name'], $theaterName) !== false;
            });
        }

        // Return the filtered movies as JSON response
        return response()->json($filteredResults);


    }

    public function searchMoviesDate(Request $request)
    {

            //API For New Movie
            $response = Http::
                        retry('1',1000) // Retry the request up to 2 times with a delay of 3 seconds between retries
                        ->get('https://821f21ea-3d75-4b17-bac5-f8a0fc587ad2.mock.pstmn.io/new_movies/?r_date=2020-01-01');

            if ($response->successful()) {

                $movies = $response->json();

                return response()->json(['data'=>$movies]);

            } else {


            //GET API FROM NEW MOVIES SPECIFIC MOVIE THEATER
            $movies = [
                [
                    "Movie_ID"=> 1,
                    "Title"=> "The Irishman",
                    "Duration"=> "1 hour 20 minutes",
                    "Views"=> "21.1k",
                    "Genre"=> "Comedy",
                    "Poster"=> "https://m.media-amazon.com/images/M/MV5BMGUyM2ZiZmUtMWY0OC00NTQ4LThkOGUtNjY2NjkzMDJiMWMwXkEyXkFqcGdeQXVyMzY0MTE3NzU@._V1_FMjpg_UX1000_.jpg",
                    "Overall_rating"=> 7.9,
                    "Theater_name"=> "ABC Movies",
                    "Start_time"=> "2020-04-04T09:00:00",
                    "End_time"=> "2020-04-04T12:00:00",
                    "Description"=> "An aging hitman recalls his time with the mob and the intersecting events with his friend, Jimmy Hoffa, through the 1950-70s.",
                    "Theater_room_no"=> 1
                ],
                [
                    "Movie_ID"=> 3,
                    "Title"=> "The Favourite",
                    "Duration"=> "1 hour 20 minutes",
                    "Views"=> "21.1k",
                    "Genre"=> "Comedy",
                    "Poster"=> "https://m.media-amazon.com/images/M/MV5BMTg1NzQwMDQxNV5BMl5BanBnXkFtZTgwNDg2NDYyNjM@._V1_.jpg",
                    "Overall_rating"=> 7.0,
                    "Theater_name"=> "ABC Movies",
                    "Start_time"=> "2020-04-04T11:00:00",
                    "End_time"=> "2020-04-04T14:00:00",
                    "Description"=> "In early 18th century England, a frail Queen Anne occupies the throne and her close friend, Lady Sarah, governs the country in her stead. When a new servant, Abigail, arrives, her charm endears her to Sarah.",
                    "Theater_room_no"=> 3
                ],
                [
                    "Movie_ID"=> 5,
                    "Title"=> "Shoplifters",
                    "Duration"=> "1 hour 20 minutes",
                    "Views"=> "21.1k",
                    "Genre"=> "Comedy",
                    "Poster"=> "https://m.media-amazon.com/images/M/MV5BYWZmOTY0MDAtMGRlMS00YjFlLWFkZTUtYmJhYWNlN2JjMmZkXkEyXkFqcGdeQXVyODAzODU1NDQ@._V1_.jpg",
                    "Overall_rating"=> 8.9,
                    "Theater_name"=> "ABC Movies",
                    "Start_time"=> "2020-04-20T09:00:00",
                    "End_time"=> "2020-04-20T12:00:00",
                    "Description"=> "A family of small-time crooks take in a child they find outside in the cold.",
                    "Theater_room_no"=> 5
                ],
                [
                    "Movie_ID"=> 2,
                    "Title"=> "Parasite",
                    "Duration"=> "1 hour 20 minutes",
                    "Views"=> "21.1k",
                    "Genre"=> "Comedy",
                    "Poster"=> "https://m.media-amazon.com/images/M/MV5BYWZjMjk3ZTItODQ2ZC00NTY5LWE0ZDYtZTI3MjcwN2Q5NTVkXkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg",
                    "Overall_rating"=> 7.2,
                    "Theater_name"=> "ABC Movies",
                    "Start_time"=> "2020-04-04T09:00:00",
                    "End_time"=> "2020-04-04T12:00:00",
                    "Description"=> "A poor family, the Kims, con their way into becoming the servants of a rich family, the Parks. But their easy life gets complicated when their deception is threatened with exposure.",
                    "Theater_room_no"=> 2
                ],
                [
                    "Movie_ID"=>4,
                    "Title"=>"The Farewell I",
                    "Duration"=>"1 hour 20 minutes",
                    "Views"=>"21.1k",
                    "Genre"=>"comedy",
                    "Poster"=>"https://m.media-amazon.com/images/M/MV5BMWE3MjViNWUtY2VjYS00ZDBjLTllMzYtN2FkY2QwYmRiMDhjXkEyXkFqcGdeQXVyODQzNTE3ODc@._V1_.jpg",
                    "Overall_rating"=>6.9,
                    "Theater_name"=>"abc movies",
                    "Start_time"=>"2020-04-04T12:00:00",
                    "End_time"=>"2020-04-04T15:00:00",
                    "Description"=>"A Chinese family discovers their grandmother has only a short while left to live and decide to keep her in the dark, scheduling a wedding to gather before she dies.",
                    "Theater_room_no"=>4
                ]
            ];

        }

        // Initialize with all movies
        $filteredResults = $movies;

        // Assuming $date and $dateEnd are retrieved from the query string
        $dateInput = $request->query('date');
        $dateEndInput = $request->query('dateEnd');

        // Create DateTime objects from the input strings
        $dateObj    = new DateTime($dateInput);
        $dateEndObj = new DateTime($dateEndInput);

        // Format DateTime objects to the desired format (with seconds included)
        $date    = $dateObj->format('Y-m-d\TH:i:s');
        $dateEnd = $dateEndObj->format('Y-m-d\TH:i:s');

        // Get theater name from query
        $theaterName = $request->query('theater_name');


        if ($dateInput !== null && $dateEndInput !== null) {
            // Filter movies based on date range and theater name
            $filteredResults = array_filter($movies, function($movie) use ($date, $dateEnd, $theaterName) {

                // Convert start time to DateTimeImmutable object
                $formattedStartDate = ($movie['Start_time']);
                // Convert end time to DateTimeImmutable object
                $formattedEndDate = ($movie['End_time']);
                // Check if movie's start date is on or after $date, end date is on or before $dateEnd, and theater name contains $theaterName
                return $formattedStartDate >= $date && $formattedEndDate <= $dateEnd && stripos($movie['Theater_name'], $theaterName) !== false;
            });

        }


            // Return JSON response containing filtered results
            return response()->json($filteredResults);

    }
}
