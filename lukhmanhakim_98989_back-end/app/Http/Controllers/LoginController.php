<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use function PHPUnit\Framework\throwException;

class LoginController
{
    public function logIn(Request $request){

        $response = Http::
                    retry('1',1000) // Retry the request up to 2 times with a delay of 3 seconds between retries
                    ->get('https://663dd111e1913c4767958526.mockapi.io/api/pcarimovie/users');

        if ($response->successful()) {

        $login = $response->json();

        return response()->json($login);

        }else{

            $login = [
                [
                    "username" => "John Glich",
                    "password" => "U2FsdGVkX1/wYfD2JS6RNoR9NGFRhF7xUotSmHtQVYM="
                ],
            ];

            return response()->json( $login);

        }


    }
}
