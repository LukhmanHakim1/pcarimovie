<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers;
use App\Http\Controllers\MoviesController;
use App\Http\Controllers\LoginController;


Route::get('/newMovie/trending',[MoviesController::class, 'index']);
Route::get('/newMovie/trending/search',[MoviesController::class, 'searchMovie']);
Route::get('/newMovie/trending/search/date',[MoviesController::class,'searchMoviesDate']);
Route::get('/logIn',[LoginController::class,'logIn']);
